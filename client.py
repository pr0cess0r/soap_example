#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pysimplesoap.client import SoapClient, SoapFault

# create a simple consumer
client = SoapClient(
    location = "http://localhost:8008/",
    action = 'http://localhost:8008/', # SOAPAction
    namespace = "http://example.com/sample.wsdl", 
    soap_ns='soap',
#    trace = True,
    ns = False)

# call the remote method
response = client.Exchange(xml='<foo/>', password='123456')

# extract and convert the returned value
result = response.result
print int(result)
