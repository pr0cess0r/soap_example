#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pysimplesoap.server import SoapDispatcher, SOAPHandler
from BaseHTTPServer import HTTPServer

from lxml import etree
from StringIO import StringIO

######## САМА ФУНКЦИЯ ########

def exchange(xml, password):
    
    PASSWORD = '123456'
    DTD = StringIO("""<!ELEMENT foo EMPTY>""")
    
    # проверка пароля
    if password != PASSWORD:
        return 1
    
    # проверка xml на правильность оформления согласно схемы DTD
    dtd = etree.DTD(DTD)
    root = etree.XML(xml)
    if not dtd.validate(root):
        return 2
        
    return 0

##############################

dispatcher = SoapDispatcher(
    'my_dispatcher',
    location = "http://localhost:8008/",
    action = 'http://localhost:8008/', # SOAPAction
    namespace = "http://example.com/sample.wsdl", prefix="ns0",
    trace = True,
    ns = True)

# register the user function
dispatcher.register_function('Exchange', exchange,
    returns={'result': int}, 
    args={'xml': str,'password': str})

print "Starting server..."
httpd = HTTPServer(("", 8008), SOAPHandler)
httpd.dispatcher = dispatcher
httpd.serve_forever()
